from django.db import models


class Game(models.Model):
    name = models.CharField(max_length=25, editable=False, blank=False)
    turn = models.IntegerField(default=1, editable=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True, editable=False)

    def __str__(self):
        return str(self.turn)


class Player(models.Model):
    nickname = models.CharField(max_length=12, blank=False)
    age = models.IntegerField(blank=False)
    money = models.IntegerField(default=15000)
    can_bet = models.BooleanField(default=True, editable=False)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True, editable=False)

    def __str__(self):
        return self.nickname


class Color(models.Model):
    name = models.CharField(max_length=15, unique=True)
    hex_code = models.CharField(max_length=7, blank=False, unique=True)
    weight = models.DecimalField(max_digits=4, decimal_places=3, null=False)
    multiplier = models.IntegerField()
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Bet(models.Model):
    amount = models.IntegerField()
    color = models.ForeignKey(Color, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.player} -> {str(self.amount)} to {self.color}"
