from django.urls import path
from game.views import *

urlpatterns = [
    path("", get_games),
    path("<int:game_id>/", game_detail),
    path("room/<int:game_id>/players/", room_players),
    path("player-detail/<int:player_id>/", player_detail),
    path("default/colors/", colors),
    path("default/color-detail/<int:color_id>/", color_detail),
    path("bet/room/<int:room_id>/player/<int:player_id>/", bet),
    path("playing/", spin_rulette),
]
