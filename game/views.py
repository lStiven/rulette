from django.core import serializers as ds
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status
from game.models import Game, Player, Bet, Color
from game.serializers import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework.permissions import IsAuthenticated
from random import choices
import re


@api_view(["GET", "POST"])
# @permission_classes([IsAuthenticated])
def get_games(request):
    if request.method == "GET":

        game = Game.objects.filter(is_active=True).order_by("-created_at")
        paginator = create_paginator(request, game, 10)

        serializer = GameSerializer(
            paginator["data"], context={"request": request}, many=True
        )

        return Response(
            {
                "data": serializer.data,
                "count": paginator["count"],
                "numpages": paginator["numpages"],
                "nextlink": paginator["nextlink"],
                "prevlink": paginator["prevlink"],
            },
            status=status.HTTP_200_OK,
        )

    if request.method == "POST":

        serializer = GameSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["GET", "PUT", "DELETE"])
# @permission_classes([IsAuthenticated])
def game_detail(request, game_id):
    try:
        game = Game.objects.get(pk=game_id, is_active=True)
    except Game.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = GameSerializer(game, context={"request": request})
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    if request.method == "PUT":
        serializer = GameSerializer(
            game,
            request.data,
            context={"request": request},
            partial=True,
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    if request.method == "DELETE":
        game.is_active = 0
        game.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(["GET", "POST"])
# @permission_classes([IsAuthenticated])
def room_players(request, game_id):
    if request.method == "GET":
        player = Player.objects.filter(
            game_id=game_id, is_active=True, game__is_active=True
        ).order_by("-created_at")
        paginator = create_paginator(request, player, 10)

        serializer = PlayerSerializer(
            paginator["data"], context={"request": request}, many=True
        )

        return Response(
            {
                "data": serializer.data,
                "count": paginator["count"],
                "numpages": paginator["numpages"],
                "nextlink": paginator["nextlink"],
                "prevlink": paginator["prevlink"],
            },
            status=status.HTTP_200_OK,
        )

    if request.method == "POST":

        data = request.data
        data["game"] = game_id
        serializer = PlayerSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["GET", "PUT", "DELETE"])
# @permission_classes([IsAuthenticated])
def player_detail(request, player_id):
    try:
        player = Player.objects.get(pk=player_id, is_active=True)
    except Player.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = PlayerSerializer(player, context={"request": request})
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    if request.method == "PUT":
        serializer = PlayerSerializer(
            player,
            request.data,
            context={"request": request},
            partial=True,
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    if request.method == "DELETE":
        player.is_active = 0
        player.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(["GET", "POST"])
# @permission_classes([IsAuthenticated])
def colors(request):
    if request.method == "GET":
        color = Color.objects.all()
        paginator = create_paginator(request, color, 10)

        serializer = ColorSerializer(
            paginator["data"], context={"request": request}, many=True
        )

        return Response(
            {
                "data": serializer.data,
                "count": paginator["count"],
                "numpages": paginator["numpages"],
                "nextlink": paginator["nextlink"],
                "prevlink": paginator["prevlink"],
            },
            status=status.HTTP_200_OK,
        )

    if request.method == "POST":

        serializer = ColorSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        match = re.search(
            r"^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$", request.data["hex_code"]
        )
        if not match:
            return Response(
                {"data": "Color code is not valid."}, status=status.HTTP_400_BAD_REQUEST
            )

        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["GET", "PUT", "DELETE"])
# @permission_classes([IsAuthenticated])
def color_detail(request, color_id):
    try:
        color = Color.objects.get(pk=color_id, is_active=True)
    except Color.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = ColorSerializer(color, context={"request": request})
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    if request.method == "PUT":
        serializer = ColorSerializer(
            color,
            request.data,
            context={"request": request},
            partial=True,
        )
        serializer.is_valid(raise_exception=True)
        match = re.search(
            r"^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$", request.data["hex_code"]
        )
        if not match:
            return Response(
                {"data": "Color code is not valid."}, status=status.HTTP_400_BAD_REQUEST
            )
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    if request.method == "DELETE":
        color.is_active = 0
        color.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(["POST"])
# @permission_classes([IsAuthenticated])
def bet(request, room_id, player_id):
    try:
        Game.objects.get(pk=room_id, is_active=True)
    except Game.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    try:
        player = Player.objects.get(pk=player_id, is_active=True)
    except Player.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    data = request.data
    data["player"] = player_id
    serializer = BetSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)

    if not player.can_bet:
        return Response(
            {
                "data": "Unable to bet rigth now, maybe you have a active bet or insufficient funds"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    if player.money - request.data["amount"] < 0:
        return Response(
            {"data": "insufficient funds"}, status=status.HTTP_400_BAD_REQUEST
        )

    player.money = player.money - request.data["amount"]
    player.can_bet = False
    player.save()
    serializer.save()

    betInstance = Bet.objects.get(pk=serializer.data["id"])
    betResponse = GetBetSerializer(betInstance)

    return Response(betResponse.data, status=status.HTTP_200_OK)


@api_view(["POST"])
# @permission_classes([IsAuthenticated])
def spin_rulette(request):

    try:
        Game.objects.get(pk=request.data["room_id"], is_active=True)
    except KeyError:
        return Response(status=status.HTTP_404_NOT_FOUND)

    except Game.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    playerId = []
    population = []
    weights = []

    players = Player.objects.filter(is_active=True, game__is_active=True)
    colors = Color.objects.filter(is_active=True)

    for player in players:
        playerId.append(player.id)

    bets = Bet.objects.filter(is_active=True, player__pk__in=playerId)

    if not bets.exists():
        return Response({"data": "No available bets"}, status.HTTP_400_BAD_REQUEST)

    if not players.exists():
        return Response({"data": "No available players"}, status.HTTP_400_BAD_REQUEST)

    for color in colors:
        population.append({color.id: color.multiplier})
        weights.append(float(color.weight))

    winner_color = choices(population, weights)
    for bet in bets:
        bet_serializer = BetSerializer(bet)
        player = Player.objects.get(pk=bet_serializer.data["player"])
        if bet_serializer.data["color"] in winner_color[0]:
            player.money += bet.amount * winner_color[0][bet_serializer.data["color"]]
            player.can_bet = True
        elif player.money > 0:
            player.can_bet = True

        bet.is_active = False
        bet.save()
        player.save()

    new_bet = Bet.objects.filter(player__pk__in=playerId)
    bet_reponse = GetBetSerializer(new_bet, many=True)

    return Response(data=bet_reponse.data, status=status.HTTP_202_ACCEPTED)


######### Local method ########
def create_paginator(request, instance, elements):
    data = []
    nextPage = 1
    previousPage = 1
    nextLink = ""
    prevLink = ""

    page = request.GET.get("page", 1)
    paginator = Paginator(instance, elements)

    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    if data.has_next():
        nextPage = data.next_page_number()

    if data.has_previous():
        previousPage = data.previous_page_number()

    nextLink = f"?page={str(nextPage)}"
    prevLink = f"?page={str(previousPage)}"

    return {
        "data": data,
        "count": paginator.count,
        "numpages": paginator.num_pages,
        "nextlink": nextLink,
        "prevlink": prevLink,
    }
