# Generated by Django 3.2.7 on 2021-10-04 06:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0009_auto_20211004_0518'),
    ]

    operations = [
        migrations.AddField(
            model_name='color',
            name='multiplier',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
