# Generated by Django 3.2.7 on 2021-10-03 21:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0003_game_modified_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='name',
            field=models.CharField(editable=False, max_length=25),
        ),
    ]
