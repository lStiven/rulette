# Generated by Django 3.2.7 on 2021-10-03 21:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0002_auto_20211003_2133'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='modified_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
